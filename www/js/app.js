// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('everBook', ['ionic', 'ion-gallery', 'ngResource', 'ngCordova', 'ngStorage', 'everBook.controllers', 'everBook.services'])

  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('tabs', {
        url: "/tab",
        abstract: true,
        templateUrl: "templates/tabs.html"
      })
      .state('tabs.home', {
        url: "/home",
        views: {
          'home-tab': {
            templateUrl: "templates/home.html",
            controller: "HomeTabCtrl"
          }
        }
      })
      .state('tabs.explore', {
        url: "/explore",
        views: {
          'explore-tab': {
            templateUrl: "templates/explore.html",
            controller: "ExploreTabCtrl"
          }
        }
      })
      .state('tabs.meetings', {
        url: "/meetings",
        views: {
          'meet-tab': {
            templateUrl: "templates/meet.html",
            controller: "MeetTabCtrl"
          }
        }
      })
      .state('tabs.meeting', {
        url: "/meetings/:meetingId",
        views: {
          'meet-tab': {
            templateUrl: "templates/meeting.html",
            controller: "MeetingCtrl"
          }
        }
      });
    $stateProvider.state('new', {
      url: '/new',
      params: {image: null, location: null},
      templateUrl: "templates/new.html",
      controller: "NewPostCtrl"
    }).state('location', {
      url: '/location',
      templateUrl: "templates/location.html"
    })
      .state('register', {
        url: '/register',
        templateUrl: "templates/register.html"
      })
      .state('login', {
        url: '/login',
        templateUrl: "templates/login.html",
        controller: "LoginCtrl"
      });
    //$urlRouterProvider.otherwise("tab/home");
    $urlRouterProvider.otherwise('login');
  })
  .directive('hideTabs', function($rootScope) {
    return {
      restrict: 'A',
      link: function(scope, element, attributes) {
        scope.$watch(attributes.hideTabs, function(value){
          $rootScope.hideTabs = value;
        });

        scope.$on('$destroy', function() {
          $rootScope.hideTabs = false;
        });
      }
    };
  })
  .constant('host', 'http://localhost:3000/api/')
  .run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      if(window.cordova && window.cordova.plugins.Keyboard) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience.
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if(window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  });
