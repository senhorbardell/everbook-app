angular.module('everBook.services', [])

  .factory('Posts', function($resource) {
    var post = {
      author: {
        id: 1,
        avatar: 'img/av.jpeg',
        name: 'Jane',
        country: 'India',
        city: 'Indore'
      },
      id: 1,
      description: 'First day at the beach! Celebrating with all of his friends',
      image: 'img/people.jpeg',
      createdAt: new Date(),
      liked: false
    };

    var posts = [];

    for (i = 0; i < 10; i++) posts.push(post);

    return {
      all: function() {
        return posts
      }
    }
  })

  .factory('Meetings', function() {
    var meeting = {
      id: 1,
      name: 'John Doe',
      avatar: 'https://randomuser.me/api/portraits/med/women/82.jpg',
      location: '0.01 miles away'
    };

    var meetings = [];

    for (i = 0; i < 10; i++) meetings.push(meeting);

    return {
      all: function() {
        return meetings
      }
    }
  })

  .factory('User', function($resource) {

  });
