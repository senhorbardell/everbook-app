angular.module('everBook.controllers', [])

  .controller('AppCtrl', function($scope, $ionicModal) {
    $ionicModal.fromTemplateUrl('templates/invite.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.inviteModal = modal;
    });

    $ionicModal.fromTemplateUrl('templates/search.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.searchModal = modal;
    });

  })

.controller('HomeTabCtrl', function($scope, $state, $ionicModal, $cordovaCamera, $localStorage, Posts) {
  console.log($localStorage.apikey);
  $scope.posts = Posts.all();
  $scope.listCanSwipe = true;
  $scope.loadMore = function() {
    // load more via $http
    console.log('Load more called');
  };

  $scope.$on('$stateChangeSuccess', function() {
    $scope.loadMore();
  });

  $scope.takePhoto = function() {
    $cordovaCamera.getPicture({
      //quality: 100,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      //allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 1080,
      //targetHeight: 200,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation: true
    }).then(function(imageData) {
      $state.go('new', {image: "data:image/jpeg;base64," + imageData})
    }, function(err) {
      alert(err);
    });
  }

})

.controller('ExploreTabCtrl', function($scope) {
  var image = {
    src: 'img/gallery-cover.jpeg',
    thumb: 'img/gallery-item.jpeg'
  };

  $scope.images = [];

  for (i = 0; i < 10; i++) $scope.images.push(image);

  $scope.cover = $scope.images[0].src;

  $scope.display = function(image) {
    $scope.cover = image.src + '#' + new Date().getTime()
  }
})

.controller('MeetTabCtrl', function($scope, $state, Meetings) {
  $scope.meetings = Meetings.all();
  $scope.listCanSwipe = true;
  $scope.loadMore = function() {
    console.log('Load more called');
  };

  //$scope.$on('$stateChangeSuccess', function() {
  //  $scope.loadMore();
  //});

})

.controller('MeetingCtrl', function($scope) {
  console.log('To the meeting');
})

.controller('NewPostCtrl', function($scope, $stateParams, $ionicHistory, $ionicModal) {
  $scope.image = $stateParams.image;
  $scope.err = $stateParams.err;

  $scope.back = function() {
    $ionicHistory.goBack();
  };

  $ionicModal.fromTemplateUrl('templates/location.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.location = modal;
  });
})

.controller('LoginCtrl', function($scope, $state, host, $http, $localStorage) {
  $scope.user = {};
  $scope.login = function() {
    $http({
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST',
      url: host + 'users/sign_in',
      data: $scope.user
    }).then(function(res) {
      // $state.go('somewhere')
      //console.log(res);
      console.log(res.data);
      $localStorage.apikey = res.data.api_key.apikey;
      $state.go('tabs.home');
    }, function(err) {
      alert(err);
    })
  }
});

